#### REST ____________________________________________________________________________

	- REST => (REpresentational State Transfer) also called a RESTful system. 

	- REST is the presentation layer state transfer, where the presentation layer is 
	  actually the resource presentation layer. 
	- So what are resources? Pictures, documents, or videos, etc. are all resources that
	  can be located by URI

	* Representation 
		- Resources are specific information entities that can be shown in a variety of
		  ways within the presentation layer. For instance a TXT document can be 
		  represented as HTML, JSON, XML, etc; and image as jpg, png, etc. 
		- URIs are used to identify resources, but how do we determine its specific 
		  manifestations? You are referred to the Accept and Content-Type in an HTTP
		  request header; these two fields describe the presentation layer. 
	* State Transfer
		- An interaction process is initiated between client and server each time you
		  visit any page of a website. During this process, certian data related to the
		  current page state need to be saved. However, you'll recall that HTTP is a 
		  stateless protocol! Its obvious that we need to save this client state, on 
		  our server side. It follows that if a client modifies some data, and wants to
		  persist the changes, there must be a way to inform the server side about the
		  new state. 
		- Most of the time, clients inform server of state changes using HTTP. They have
		  four operations with which to do this. 
			- GET is used to obtain resources 
			- POST is used to create or update resources
			- PUT updates resources
			- DELETE deletes resouces
	* Summary
		1) Every URI represents a resource
		2) There is a representation layer for transferring resources between clients
		   and servers. 
		3) Clients use four HTTP methods to implement "Presentation Layer State Transfer", 
		   allowing them to operate on remote resources. 

	- The most important principle of web applications that implement REST is that the 
	  interaction between clients and servers are stateless; every request should 
	  encapsulate all of the required information. Servers should be able to restart at
	  any time without he clients being notified. In addition, requests can be 
	  responded by any server of the same service, which is ideal for cloud computing. 
	  Lastly, because its stateless, clients can cache data for improving preformance. 

	- Another important principle of REST is system delaminatoin, which means that 
	  components in one layer have no way of interaction directly with components in other
	  layers. This can limit system complexity and encourage independence in the 
	  underlying components. 

***RESTful Implimentation***

	- Go doesn't have direct support for REST, but since RESTful web applications are all 
	  HTTP-based, we can use the <net/http> package to implement it. Some modifications
	  necessary. 

	EX.1 RESTful Architecture

		package main

		import (
			"fmt"
			"github.com/julienschmidt/httprouter"
			"log"
			"net/http"
		)

		func Index(w http.ResponseWriter, r *http.Request, _httprouter.Params) {
			fmt.Fprint(w, "Welcome!\n")
		}

		func Hello(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
			fmt.Fprintf(w "hello, %s!\n", ps.ByName("name"))
		}

		func getuser(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
			uid := ps.ByName("uid")
			fmt.Fprintf(w, "you are get use %s", uid)
		}

		func modifyuser(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
			uid := ps.ByName("uid")
			fmt.Fprintf(w, "you are modify use %s", uid)
		}

		func deleteuser(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
			uid := ps.ByName("uid")
			fmt.Fprintf(w, "you are delete user %s", uid)
		}

		func adduser(w http.ReponseWriter, r *http.Request, ps httprouter.Params) {
			// uid := r.FormValue("uid")
			uid := ps.ByName("uid")
			fmt.Fprintf(w, "you are add user %s", uid)
		}

		func main() {
			router := httprouter.New()
			router.GET("/", Index)
			router.GET("/hello/:name", Hello)

			router.GET("/user/:uid", getuser)
			router.POST("/adduser/:uid", adduser)
			router.DELETE("/deluser/:uid", deleteuser)
			router.PUT("/moduser/:uid", modifyuser)

			log.Fatal(http.ListenAndServe(":8080", router)
		}

	EX.1-A
		- The sample code shows you how to write a very basic REST application. Our 
		  resources are users, and we use different functions for different methods. 
		  Here we imported a third-party package called <github.com/julienschmidt/httprouter>


	- REST is a style of web architecture, building on past successful WWW, statelessness, 
	  resource-centric, full use of HTTP and URI protocols, and the provision of unified
	  interfaces. 



























