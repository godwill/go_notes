#### Sockets ________________________________________________________________________________

	The two main protocols are REST and SOAP

	1) REST: requests are pretty straight forward because it's based on HTTP. Every Rest
	         request is actually an HTTP request, and servers handle requests using different
			 methods. 
	2) SOAP: is a standard for cross-network information transmission and remote computer
	         function calls, launched by W3C. The problem with SOAP is that its specification
			 is very long and complicated, and its stil getting longer. 

	What is a Socket: 
		- sockets originate from Unix, and given the basic, "everything is a file" philosophy 
		  of Unix, everything can be operated on with "open -> write/read -> close". Sockets
		  are one implementation of this philosophy. Sockets have a function call for opening
		  a socket just like you would open a file. This returns an int descriptor of the 
		  socket which can then be used for operations like creating connections, transferring
		  data, etc. 
		- Two types of sockets that are commonly used are
			1) <SOCK_STREAM> for streaming sockets
			2) <SOCK_DGRAM> for datagram sockets
		- Stream sockets are connection-oriented like TCP, while datagram sockets do not 
		  establish connections, like UDP. 

***Socket Basic Knowledge***

	- sockets have two types, TCP sockets, and UDP sockets. TCP and UDP are protocols and, as
	  mentioned, we also need an IP address and port number to have a unique socket. 

	1) IPv4:
		- The global internet uses TCP/IP as its protocol, where IP is the network layer, and
		  a core part of TCP/IP. IPv4 signifies that its version 4; infrastructure development
		  to date has spanned over 30 years. 
		- The number of bits in an IPv4 address is 32, which means that 2^32 decives are able
		  to uniquely connect to the internet. Due to rapid development of the internet, IP
		  addresses are already running out of stock in recent years
		- Address format: <127.0.0.1>, <172.122.121.111> 

	2) IPv6: 
		- IPv6 have an address that's 128 bits long, so we'll never need to worry about a 
		  shortage of unique addresses. 
		- Other problems arise with IPv6 like peer to peer connection, service quality (QoS), 
		  security, multiple boradcast, ect. 
		- Address format: <2002:c0e8:82e7:0:0:0:c0e8:82e7>

***Ip Types in Go***

	- The <net> package in Go provides many types, functions and methods of network programming. 

	The definition of IP: <type IP []byte> 

	EX.1 Function <ParseIP(s string) IP> is to convert IPv4 to IPv6 format

		package main

		import (
			"net"
			"os"
			"fmt"
		)

		func main() {
			if len(os.Args) != 2 {
				fmt.Fprintf(os.Stderr, "Usage: %s ip-addr\n", os.Args[0])
				os.Exit(1)
			}
			name := os.Args[1]
			addr := net.ParseIP(name)
			if addr == nil {
				fmt.Println("Invalid Address")
			} else {
				fmt.Println("The address is ", add.String())
			}
			os.Exit(0)
		}

	EX.1-A
		- Returns the corresponding IP format for a given IP address. 

***TCP Socket***

	- As a client, we can send a request to an appointed network port and gets its response; 
	  as a server we need to bind a servie to an appointed network port, wait for clients'
	  request and supply a response. 

	- <net> package, there's a types called <TCPConn> that facilities this kind of clients/
	  servers interaction. 
	This type has two key functions: 
	  1) <func (c *TCPConn) Write(b []byte) (n int, err os.Error)>
	  2) <func (c *TCPConn) Read(b []byte) (n int, err os.Error)>

	EX.2 You'll also need a <TCPAddr> to represent TCP address information

		type TCPAddr struct {
			IP 		IP
			Port 	int
		}

		// we use <ResolveTCPAddr> function to get a <TCPAddr>

		func ResolveTCPAddr(net, addr string) (*TCPAddr, os.Error) 

	EX.2-A
		- Arguments of <net> can be one of "tcp4", or "tcp", which each signify IPv4-only, 
		  IPv6-only, and either IPv4 or IPv6 respectively. 
		- <addr> can be a domain name or IP address, like "www.google.com:89" or "127.0.0.1:22"

***TCP Client***

	- go clients use <DialTCP> function in the <net> package to create a TCP connection, which
	  returns a <TCPConn> object; after a connection is established, the server has the same
	  type of connection object for the current connection, and client and server can begin
	  exchanging data with one another. 
	- In general, clients send requests to server through <TCPConn> and recieve information from
	  the server response; servers read and parse client requests, then return feedback. This
	  connection will remain valid until either the client or server closes it. The function
	  for create a connection is as follows. 

	EX.3 Function for creating a connection

		func DialTCP(net string, laddr, raddr *TCPAddr) (c *TCPConn, err os.Error) 

	EX.3-A
		- Arguments of <net> can be one of "tcp4", "tcp6", or "tcp"
		- <laddr> represents the local address, set it to <nil> in most cases. 
		- <raddr> represents the remote address. 

	EX.4 Sample Request

		package main 

		import (
			"fmt"
			"ip/ioutil"
			"net"
			"os"
		)

		func main() {
			if len(os.Args) != 2 {
				fmt.Fprintf(os.Stderr, "Usage: %s host:port ", os.Args[0])
				os.Exit(1)
			}
			service := os.Args[1]
			tcpAddr, err := net.ResolveTCPAddr("tcp4", service)
			checkError(err)
			conn, err := net.DialTCP("tcp", nil, tcpAddr)
			checkError(err)
			_, err = conn.Write([]byte("HEAD / HTTP/1.0\r\n\r\n"))
			checkError(err)
			result, err := ioutil.ReadAll(conn)
			checkError(err)
			fmt.Println(string(result))
			os.Exit(0)
		}

		func checkError(err error) {
			if err != nil {
				fmt.Fprintf(os.Stderr, "Fatal error: %s", err.Error())
				os.Exit(1)
			}
		}

	EX.4-A
		- we use user input as the <service> argument of <net.ResolveTCPAddr> to get a <tcpAddr>
		  passing <tcpAddr> to the <DialTCP> function, we create a TCP connection, <conn>. We
		  can then use <conn> to send requestion information to the server. Finally we use
		  <ioutil.ReadAll> to read all the content from <conn>, which contains the server 
		  response. 

***TCP Server***

	- We have a TCP client now. We can also use the <net> package to write a TCP server. On the 
	  server side, we need to bind our service to a specific inactive port and listen for any
	  incoming client requests. 

	Server side binding functions: 

		func ListenTCP(net string, laddr *TCPAddr) (l *TCPListener, err os.Error)
		func (l *TCPListener) Accept() (c Conn, err os.Error)

	EX.5 Syncing Service using port 7777

		package main 

		import (
			"fmt"
			"net"
			"os"
			"time"
		)

		func main() {
			service := ":7777"
			tcpAddr, err := net.ResolveTCPAddr("tcp", tcpAddr)
			checkError(err)
			listener, err := net.ListenTCP("tcp", tcpAddr)
			checkError(err)
			for {
				conn, err := listener.Accept()
				if err != nil {
					continue
				}
				daytime := time.Now().String()
				conn.Write([]byte(daytime)) 	// don't care about return value
				conn.Close()					// we're finished with this client
			}
		}

		func CheckError(err Error) {
			if err != nil {
				fmt.Fprintf(os.Stderr, "Fatal error: %s", err.Error())
				os.Exit(1)
			}
		}

	EX.5-A
		- When it receives a client request, it <Accept>'s it and returns a response to the 
		  client containing information about the current time. It's worth noting that when
		  erros occur in the <for> loop, the service continues running instead of exiting. 
		  Instead of crashing, the server wil record the error to the server error log. 
		- The above code is still not good enough, we didn't make use of goroutines, which
		  would have allowed us to accept simultaneous requests. 

	EX.5 Simultaneous Request Handler

		package main

		import (
			"fmt"
			"net"
			"os"
			"time"
		)

		func main() {
			service := ":1200"
			tcpAddr, err := net.ResolveTCPAddr("tcp4", service)
			checkError(err)
			listener, err := netListenTCP("tcp", tcpAddr)
			checkError(err)
			for {
				conn, err := listener.Accept()
				if err != nil {
					continue 
				}
				go handleClient(conn)
			}
		}

		func handleClient(conn net.Conn) {
			defer conn.Close()
			daytime := time.Now().String()
			conn.Write([]byte(daytime)) 	// don't care about return value
			// we're finished with this client
		}

		func checkError(err Error) {
			if err != nil {
				fmt.Fprintf(os.Stderr, "Fatal error: %s", err.Error())
				os.Exit(1)
			}
		}

	EX.5-A
		- Separating out our business process from <handleClient> function, and by using the
		  <go> keyword, we've already implemented concurrency in our service. 

	EX.6 Multiple Requests for different time formats over a single connection

		package main

		import (
			"fmt"
			"net"
			"os"
			"time"
			"strconv"
		)

		func main() {
			service := ":1200"
			tcpAddr, err := net.ResolveTCPAddr("tcp4", service)
			checkError(err)
			listener, err := net.ListenTCP("tcp", tcpAddr)
			checkError(err)
			for {
				conn, err := listener.Accept()
				if err != nil {
					continue
				}
				go handleClient(conn)
			}
		}

		func handleClient(conn net.Conn) {
			conn.SetReadDeadline(time.Now().Add(2 * time.Minute)) // set 2 min timout
			request := make([]byte, 128) // set maximum request length to 128b to prevent
										 // flood based attacks
			defer conn.Close() // close connection before exit
			for {
				read_len, err := conn.Read(request)

				if err != nil {
					fmt.Println(err)
					break
				}

				if read_len == 0 {
					break // connection already closed by client
				} else if string(request[:read_len]) == "timestamp" {
					daytime := strconv.FormatInt(time.Now().Unix(), 10)
					conn.Write([]byte(daytime))
				} else {
					daytime := time.Now().String()
					conn.Write([]byte(daytime))
				}
			}
		}

		func checkError(err Error) {
			if err != nil {
				fmt.Fprintf(os.Stderr, "fatal error: %s", err.Error())
				os.Exit(1)
			}
		}

	EX.6-A
		- In this example we use <conn.Read()> to constantly read client requests. We cannot
		  close the connect because clients may issue more than one request. Due to the timeout
		  we set using <conn.SetReadDeadline()>, the connection closes automatically after our
		  allotted time period. When the expiry time has elapsed, our program breaks from the 
		  <for> loop. Notice that <request> needs to be created with a max size limitation in
		  order to prevent flood attacks. 

***Controlling TCP Connections***

	- Controlling TCP Functions: 
			
		func DialTimeout(net, addr string, timeout time.Duration) (Conn, error) 

	- Setting the timeout of connections. These are suitable for use on both clients and servers: 

		func (c *TCPConn) SetReadDeadline(t time.Time) error
		func (c *TCPConn) SetWriteDeadline(t time.Time) error

	- Setting the write/read timeout of one connection: 

		func (c *TCPConn) SetKeepAlive(keepalive bool) os.Error

	- Long connections can reduce the amount of overhead involved in creating connections and 
	  are good for applications that need to exchange data frequently. 

	-> Checkout <net> package from GO. 

***UDP Sockets***

	- The only difference between a UDP socket and a TCP socket is the processing method for
	  dealing with multiple requests on server side. This arises from the face that UDP does
	  not have a function like <Accept>. All of the other functions have <UDP> counterparts: 
	  just replace <TCP> with <UDP> in the functions above. 

	EX.7 UDP Functions

		func ResolveUDPAddr(net, addr string) (*UDPAddr, os.Error) 
		func DialUDP(net string, laddr, raddr *UDPAddr) (c *UDPConn, err os.Error)
		func ListenUDP(net string, laddr *UDPAddr) (c *UDPConn, err os.Error)
		func (c *UDPConn) ReadFromUDP(b []byte) (n int, addr *UDPAddr, err os.Error)
		func (c *UDPConn) WiteToUDP(b []byte, addr *UDPAddr) (n int, err os.Error)

	EX.8 UDP Client Code

		package main

		import (
			"fmt"
			"net"
			"os"
		)

		func main() {
			if len(os.Args) != 2 {
				fmt.Fprintf(os.Stderr, "Usage: %s host:port", os.Args[0])
				os.Exit(1)
			}
			service := os.Args[1]
			udpAddr, err := net.ResolveUDPAddr("upd4", service)
			checkError(err)
			conn, err := net.DialUDP("udp", nil, udpAddr)
			checkError(err)
			_, err = conn.Write([]byte("anything"))
			checkError(err)
			var buf [512]byte
			n, err := conn.Read(buf[0:])
			checkError(err)
			fmt.Println(string(buf[0:n]))
			os.Exit(0)
		}

		func checkError(err Error) {
			if err != nil {
				fmt.Fprintf(os.Stderr, "fatal error: ", err.Error())
				os.Exit(1)
			}
		}

	EX.9 UDP Server Code 

		package main 

		import (
			"fmt"
			"net"
			"os"
			"time"
		)

		func main() {
			service := ":1200"
			udpAddr, err := net.ResolveUDPAddr("udp4", service)
			checkError(err)
			conn, err := net.ListenUDP("udp", udpAddr)
			checkError(err)
			for {
				handleClient(conn)
			}
		}

		func handleClient(conn *net.UDPConn) {
			var buf [512]byte
			_, addr, err := conn.ReadFromUDP(buf[0:])
			if err != nil {
				return 
			}
			daytime := time.Now().String()
			conn.WriteToUDP([]byte(daytime), addr)
		}

		func checkError(err Error) {
			if err != nil {
				fmt.Fprintf(os.stderr, "fatal error: ", err.Error())
				os.Exit(1)
			}
		}



















