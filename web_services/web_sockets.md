#### Web Sockets __________________________________________________________________________

	- Browsers like Firefox, Google-Chrome, and Safari provide support for websockets
	
	- Primary Advantages: 
		- Only one TCP connection for a single web client
		- WebSocket servers can push data to web clients
		- Lightweight header to reduce data transmission overhead
	
	- WebSocket URLs begin with ws:// or wss://(SSL) 

	- WebSocket protocol: After successfully completing the initial handshake, a connection
	  is established. Subsequent data communications will all begin with 
	  	- "\x00"
		- "\xFF"
	  This prefix and suffix will be visible to clients because the WebSocket will break
	  off both end, yielding the raw data automatically. WebSocket connections are 
	  requested by browsers, and responded to by servers. After which the connection 
	  is established. 

***Sec-WebSocketKey***
	
	- This key is generated randomly and is base64 endocded. Servers need to append this key
	  to a fixed string after accepting a request: 
	  	EX. 234JLKH234-2342KJH-345KJHK-KJH234

	Example: Generated Key: (258EAFA5-E914-47DA-95CA-C5AB0DC85B11)
		- Supose you have (f7cb4ezEAl6C3wRaU6JORA==)
			then: (f7cb4ezEAl6C3wRaU6JORA==258EAFA5-E914-47DA-95CA-C5AB0DC85B11)
		- Use SHA1 to compute the binary value and use base64 to encode it
			then: rE91AJhfC+6JdVcVXOGJEADEJdQ=
		- Use (rE91AJhfC+6JdVcVXOGJEADEJdQ=) as the value of the <Sec-WebSocket-Accept> 

***Go WebSockets***

	- WebSockets are not native on Go -> you must use the <websocket> package. 

	EX.1 install websockets library
		
		go get golang.org/x/net/websocket

	- Websockets have both client side and server side, and sends it to the server through
	  a websocket, followed by the server pushing information back to the client. 

	- Clients attempting to open a connection typically bind to four events: 
		1) onopen: triggered after connection has been established
		2) onmessage: triggered after recieving a message
		3) onerror: triggered after an error has occurred
		4) onclose: triggered after the connection has closed

 (See: go/src/example-projects/websockets)















































