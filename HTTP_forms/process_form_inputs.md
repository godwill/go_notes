#### Processing Form Inputs ____________________________________________________________

	EX.1 Simple Example of a typical user user, saved as <login.gtpl> 

		<html> 
			<head>
			<title></title>
			</head>
			<body>
				<form action="/login" method="post">
					Username:<input type="text" name="username">
					Password:<input type="password" name="password">
					<input type="submit" value="Login">
				</form>
			</body>
		</html>

	EX.1-A
		- This form will submit to </login> on the server. After the users clicks the
		  login buddton, the data will be sent to the <login> handler registered by
		  the server router. Then we need to know whether is uses the POST method
		  or GET. 

	EX.2 Simple Form Login Page

		package main

		import (
			"fmt"
			"html/template"
			"log"
			"net/http"
			"strings"
		)

		func sayhelloName(w http.ResponseWriter, r *http.Request) {
			r.ParseForm() 	// parse URL params passed, then parse the response packet
							// for the POST body (request body)
			// attention: If you do not call ParseForm method, the following data can
			//            not be obtained form
			fmt.Println(r.Form) 	// print information on server side
			fmt.Println("path", r.URL.Path)
			fmt.Println("scheme", r.URL.Scheme)
			fmt.Println(r.Form["url_long"])
			for k, v := range r.Form {
				fmt.Println("Key: ", k)
				fmt.Println("Val: ", strings.Join(v, ""))
			}
			fmt.Fprintf(w, "Hello Server!") 	// write data to response 
		}

		func login(w http.ResponseWriter, r *http.Request) {
			fmt.Println("method: ", r.Method) 	// get request method
			if r.Method == "GET" {
				t, _ := template.ParseFiles("login.gtpl")
				t.Execute(w, nil)
			} else {
				r.ParseForm()
				// logic part of log in
				fmt.Println("username: ", r.Form["username"])
				fmt.Println("password: ", f.Form["password"])
			}
		}

		func main() {
			http.HandleFunc("/", sayhelloName) 	// setting router rule
			http.HandleFunc("/login", login)
			err := http.ListenAndServe(":9090", nil) 	// setting listening port
			if err != nil {
				log.Fatal("ListenAndServe: ", err)
			}
		}

	EX.2-A Explination of EX.2 code
		- here we use <r.Method> to get the request method, and it returns an http
		  verb - "GET", "POST", "PUT", ect. 
		- In the <login> function we use <r.Method> to check whether its a login page
		  or login processing logic. In other words, we check to see whether the user
		  is simply opening the page, or trying to log in. Serve shows the page only
		  when the request comes in via the "GET" method, and it executes the login
		  logic when the request uses the "POST" method. 

	EX.3 request.Form is url.Value. It saves data with formate key=value

		v := url.Values{}
		v.Set("name", "Ava")
		v.Add("friend", "Jess")
		v.Add("friend", "Sarah")
		v.Add("friend", "Zoe")
		// v.Encode() == "name=Ava&friend=Jess&friend=Sarah&friend=Zoe"
		fmt.Println(v.Get("name"))
		fmt.Println(v.Get("friend"))
		fmt.Println(v["friend"])

	Tip: 
		- Requests have the ability to access form data using the <FormValue()> method. 
		  For Example, you can change <r.Form["username"] to <r.FormValue("username")>,
		  and Go calls <r.ParseForm> automatically. Notice that it returns the first
		  value if there are arguments with the same name, and it returns an empty
		  string if there is no such argument. 




























