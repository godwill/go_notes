#### Simple Web Server __________________________________________________________________

	- HTTP package <net/http>

	EX.1 Simple Web Server

		package main

		import (
			"fmt"
			"net/http"
			"strings"
			"log"
		)

		fun sayhelloName(w http.ResponseWritter, r *http.Request) {
			r.ParseFor()	// parse arguments, you have to call this by yourself
			fmt.Println(r.Form) 	// print form information in server side
			fmt.Println("path", r.URL.Path)
			fmt.Println("scheme", r.URL.Scheme)
			fmt.Println(r.Form["url_long"])
			for k, v := range r.Form {
				fmt.Println("key:", k)
				fmt.Println("val:", strings.Join(v, ""))
			}
			fmt.Fprintf(w, "Hello astaxie!") 	// send data to client side
		}

		func main() {
			http.HandleFunc("/", sayhelloName) 	// set router
			err := http.ListenAndServe(":9090", nil) 	// set listen port 
			if err != nil {
				log.Fatal("ListenAndServe: ", err)
			}
		}

	EX.1-A
		- Code serves up response to <http://localhost:9090>, and the screen should 
		  print <Hello astaxie!>







































