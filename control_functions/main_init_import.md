#### Main, Init __________________________________________________________________________


Go has two retentions which are called main and init, where init can be used in all packages and main can only be used in the main package. These two functions are not able to have arguments or return values. Even though we can write many init functions in one package, I strongly recommend writing only one init function for each package.
Go programs will call init() and main() automatically, so you don't need to call them by yourself. For every package, the init function is optional, but package main has one and only one main function.
Programs initialize and begin execution from the main package. If the main package imports other packages, they will be imported in the compile time. If one package is imported many times, it will be only compiled once. After importing packages, programs will initialize the constants and variables within the imported packages, then execute the init function if it exists, and so on. After all the other packages are initialized, programs will initialize constants and variables in the main package, then execute the init function inside the package if it exists. The following figure shows the process.



#### Import _______________________________________________________________________________

	- Go supports third party packages in two ways. 
		1) Relative path import "./model" // load package in the same directory, I don't recommend
		   this way. 
		2) Absolute path import "shorturl/model" // load package in path "$GOPATH/pkg/shorturl/model"

	- There are special operators when we import packages. 

	1. Dot Operator 
		- The dot operator means you can omit the package name when you call functions inside of that
		  package. 

		  EX.1 

		  	import( 
			  .  "fmt"
			) 

			// Now <fmt.PrintF("hello") becomes <Printf("hello")> 

	2. Alias Operation 
		- It changes the name of the package that we imported when we call functions that belong 
		  to that package. 

		EX.1 
			
			import(
			  f "fmt"
			)

		  // Now <fmt.Printf("hello") becomes<f.Printf("hello") 

	3. < _ > (underline) Operator
		- The < _ > operator actually means we just want to import that package and execute its
		  <init> function, and we are not sure if we want to use the functions belonging to that 
		  package. 

		EX.1 

			import ( 
				"database/sql"
				_ "github.com/ziutek/mymysql/godrv"
			) 

























