
#### Functions _________________________________________________________________________________

	- You use the <func> keyword to define a function

	EX.1 Func Syntax
		
		func funcName(input1 type1, input2 type2) (output1 type1, output2 type2) {
			// function body
			// multi-value return 
			return value1, value2
		}

	EX.1-A Extrapolation
		- <func> defined function is <funcName>
		- Functions have zero, one or more that one argument. The argument type comes after
		  the argument name and arguments are separated by <,> 
		- Functions can return multiple values
		- There are two return values named <output1>, and <output2>, you can omit their names
		  and use their type only
		- If there is only one return value and you omitted the name, you don't need brackets
		  for the return values
		- if the function doesn't have return values, you can omit the return params althougher
***		- If the function has return values, you have to use the <return> statement somewhere
***		  in the body of the function

	EX.2 

		package main 

		import "fmt"

		// return greater value between a and b
		func max(a, b int) int {
			if a > b {
				return a
			}
			return b
		} 

		func main() {
			x := 3
			y := 4
			z := 5

			max_xy := max(x, y) // call function max(x, y)
			max_xz := max(x, z) // call function max(x, z)

			fmt.Printf("max(%d, %d) = %d\n, x, y, max_xy)
			fmt.Printf("max(%d, %d) = %d\n, x, z, max_xz) 
			fmt.Printf("max(%d, %d) = %d\n, y, z, max(y, z)) // call function here
		} 

	EX.2-A Explaination 
		- In the above function <max> there are two arguments both with type <int> so the first
		  type can be omitted. 
		  	A. Example: <a, b int> instead of <a int, b int> 
				- The same rules apply for additional arguments
		- Notice here that <max> only has one return value, so we only need to write the type 
		  of its return value - this is the short form of writing it. 

	EX.3 Multi-Value Return 
		
		package main 

		import "fmt"

		func SumAndProduct(A, B int) (int, int) {
			return A + B, A * B
		}

		func main() {
			x := 3
			y := 4

			xPLUSy, xTIMESy := SumAndProduct(x, y) 

			fmt.Printf("%d + %d = %d\n", x, y, xPLUSy)
			fmt.Printf("%d + %d = %d\n", x, y, xTIMESy) 
		} 

	EX.3-A Explaination 
		- The function <SumAndProduct> returns two values without names -you have the option of
		  naming them also. If you name the return values, you will need to use <return> to 
		  return the values since they are initialized in the function automatically.
*		  <GLOBAL FUNCTIONS> 
		- Notice that if your functions are going to be used outside of the package, which means
		  your function names start with a capital letter, you'd better write complete statements
		  for <return>; i makes the code more readable. 

	EX.4 Global Functions 

		// global function using capital function name 
		func SumAndProduct(A, B int) (add int, multiplied int) {
			add = A + B				// return values must be used
			multiplied = A * B		// return value must be used
			return 					// Must <return> function
		}


		

